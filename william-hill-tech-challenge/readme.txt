﻿General Info:
--------------

1. I decided to use MVC for the UI as this is what I've been coding in most recently, and I figured it would be the fastest
   way to get something up and running. It would perhaps have been easier to simply create a Desktop App but I don't think
   it makes much difference to the problem.

2. I decided to create this as a full Web Application using a database to store the data, with the idea that the program
   should to some degree reflect what would need to be implemented in a larger system where there is lots of data, and the
   data can be added to in the future (the database design reflects this which I can discuss more later). Part way through 
   I realized that including a database for this small of a problem was a bit overkill and it probably would have made more
   sense to have just done everything in memory, but I decided to stick with the database, since that's what I started off
   with.

   Note the data is loaded from the files into the database the first time the application is run.

3. The UI simply lists the Bet info for settled or unsettled bets with flags denoting bets with high risk characteristics.
   I also show the Customer statistical info that was calculated as this would likely be useful to users.

Build Info:
------------

Since this is a web application with a database backend, it does require some work to set up. To build & run the solution:
1. Open the Solution in Visual Studio 2013 or higher
2. Build the Solution (I haven't checked in the binaries)
3. Run the GenerateDatabase script (in the SQL folder) on an SQL Database Server instance
4. Update the "BetsEntities" ConnectionString in the Web.config file to point to the Database Server used
5. Run the project from Visual Studio (or set up an IIS website to run it)

Next Steps:
------------

If I had more time I planned on adding the following:

1. Paging (not really necessary for now as there is so little data, but would be essential for a larger application)

2. Search/Filter and Sorting of the data (by all of the columns)

3. Consider a better way to initially load the data into the Database (e.g. background process) - NB: this seems highly
   unnecessary for this problem
