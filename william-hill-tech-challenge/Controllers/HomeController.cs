﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using william_hill_tech_challenge.Data;
using william_hill_tech_challenge.Models;

namespace william_hill_tech_challenge.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Displays all Settled or Unsettled Bets
        /// </summary>
        /// <param name="Settled"></param>
        /// <returns></returns>
        public ActionResult Index(bool? Settled)
        {
            HomeModel model = new HomeModel { Settled = Settled.HasValue ? Settled.Value : true };
            SetBets(model);
            return View(model);
        }

        /// <summary>
        /// Displays all Customers Bet History Statistics
        /// </summary>
        /// <returns></returns>
        public ActionResult Customers()
        {
            CustomersModel model = new CustomersModel();
            SetCustomers(model);
            return View(model);
        }

        private void SetBets(HomeModel model)
        {
            using (BetsEntities entities = new BetsEntities())
            {
                var bets =
                    from bet in entities.Bets
                    where bet.Settled == model.Settled
                    join customer in entities.Customers on bet.CustomerId equals customer.CustomerId
                    select new { Bet = bet, Customer = customer };

                model.Bets = bets.ToList().Select(bet => new BetModel(bet.Bet, bet.Customer));
            }
        }

        private void SetCustomers(CustomersModel model)
        {
            using (BetsEntities entities = new BetsEntities())
            {
                model.Customers = entities.Customers.ToList().Select(cust => new CustomerModel(cust));
            }
        }
    }
}