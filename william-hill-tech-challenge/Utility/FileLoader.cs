﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using william_hill_tech_challenge.Data;

namespace william_hill_tech_challenge.Utility
{
    public class FileLoader
    {
        /// <summary>
        /// Loads the data contained in the Settled & Unsettled files from the
        /// specified folder into the database
        /// </summary>
        /// <param name="fileLocation"></param>
        public static void LoadFiles(string fileLocation)
        {
            // Check if data has already been loaded
            using (BetsEntities entities = new BetsEntities())
            {
                if (entities.Bets.Any())
                {
                    return;
                }
            }

            // Load data
            LoadedData settledData = LoadFileToMemory(Path.Combine(fileLocation, "Settled.csv"), true);
            SaveSettledDataToDatabase(settledData);

            LoadedData unsettledData = LoadFileToMemory(Path.Combine(fileLocation, "Unsettled.csv"), false);
            SaveUnsettledDataToDatabase(unsettledData);
        }

        // --- LOAD/PARSE FILE ---

        private static LoadedData LoadFileToMemory(string filePath, bool settled)
        {
            List<Bet> bets = new List<Bet>();
            Dictionary<int, Customer> customers = new Dictionary<int, Customer>();

            // --- PARSE CSV FILE ---

            StreamReader file = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filePath));

            string line = file.ReadLine(); // throw away header row
            if (line == null)
                throw new FormatException(string.Format("File {0} in incorrect format", filePath));

            while ((line = file.ReadLine()) != null)
            {
                string[] betData = line.Split(',');
                if (betData.Length != 5)
                    throw new FormatException(string.Format("File {0} in incorrect format", filePath));

                Bet bet = new Bet
                {
                    Settled = settled,
                    CustomerId = int.Parse(betData[0]),
                    EventId = int.Parse(betData[1]),
                    ParticipantId = int.Parse(betData[2]),
                    Stake = int.Parse(betData[3]),
                    WinValue = int.Parse(betData[4])
                };
                bets.Add(bet);

                if (!customers.ContainsKey(bet.CustomerId))
                {
                    customers.Add(bet.CustomerId, new Customer
                    {
                        CustomerId = bet.CustomerId
                    });
                }

                // Compute customer total bet history stats (not relevant for unsettled bets)
                if (settled)
                {
                    Customer customer = customers[bet.CustomerId];
                    customer.NumBets += 1;
                    customer.NumWins += (bet.WinValue > 0 ? 1 : 0);
                    customer.TotalBetValue += bet.Stake;
                }
            }

            file.Close();

            if (bets.Count == 0)
                throw new FormatException(string.Format("File {0} contained no data", filePath));

            return new LoadedData
            {
                Bets = bets,
                Customers = customers.Values
            };
        }

        // --- SAVE TO DATABASE ---

        private static void SaveSettledDataToDatabase(LoadedData data)
        {
            using (BetsEntities entities = new BetsEntities())
            {
                // Add customers
                entities.Customers.AddRange(data.Customers.OrderBy(cust => cust.CustomerId));
                entities.SaveChanges();

                // Add Bets
                entities.Bets.AddRange(data.Bets);
                entities.SaveChanges();
            }
        }

        private static void SaveUnsettledDataToDatabase(LoadedData data)
        {
            using (BetsEntities entities = new BetsEntities())
            {
                // Add any new customers that weren't already present in the database
                foreach (Customer customer in data.Customers.OrderBy(cust => cust.CustomerId))
                {
                    if (!entities.Customers.Any(cust => cust.CustomerId == customer.CustomerId))
                    {
                        entities.Customers.Add(customer);
                    }
                }

                // Add bets
                entities.Bets.AddRange(data.Bets);
                entities.SaveChanges();
            }
        }

        // --- HELPER CLASS ---

        public class LoadedData
        {
            public IEnumerable<Bet> Bets;
            public IEnumerable<Customer> Customers;
        }
    }
}