﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using william_hill_tech_challenge.Data;

namespace william_hill_tech_challenge.Models
{
    public class HomeModel
    {
        public bool Settled { get; set; }
        public IEnumerable<BetModel> Bets { get; set; }
    }

    public class BetModel
    {
        public BetModel(Bet bet, Customer customer)
        {
            Bet = bet;

            Risky = (customer.NumWins / (float)customer.NumBets > 0.6f);
            Unusual = (!bet.Settled && bet.Stake > 10 * (customer.TotalBetValue / (decimal)customer.NumBets));
            HighlyUnusual = (!bet.Settled && bet.Stake > 30 * (customer.TotalBetValue / (decimal)customer.NumBets));
            LargeBet = (!bet.Settled && bet.WinValue >= 1000);
        }

        public Bet Bet { get; set; }

        public bool Risky { get; set; }

        public bool Unusual { get; set; }

        public bool HighlyUnusual { get; set; }

        public bool LargeBet { get; set; }
    }
}