﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using william_hill_tech_challenge.Data;

namespace william_hill_tech_challenge.Models
{
    public class CustomersModel
    {
        public IEnumerable<CustomerModel> Customers { get; set; }
    }

    public class CustomerModel
    {
        public CustomerModel(Customer customer)
        {
            Customer = customer;

            Risky = (customer.NumWins / (float)customer.NumBets > 0.6f);
        }

        public Customer Customer { get; set; }

        public bool Risky { get; set; }
    }
}