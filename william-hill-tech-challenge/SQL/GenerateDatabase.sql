﻿USE [master];
GO

CREATE DATABASE [Bets];
GO

USE [Bets];
GO

CREATE TABLE [dbo].[Bet](
	[BetId] [int] IDENTITY(1,1) NOT NULL,
	[Settled] [bit] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[ParticipantId] [int] NOT NULL,
	[Stake] [decimal](18, 2) NOT NULL,
	[WinValue] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Bet] PRIMARY KEY CLUSTERED 
(
	[BetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] NOT NULL,
	[NumBets] [int] NOT NULL,
	[NumWins] [int] NOT NULL,
	[TotalBetValue] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Bet]  WITH CHECK ADD  CONSTRAINT [FK_Bet_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO

ALTER TABLE [dbo].[Bet] CHECK CONSTRAINT [FK_Bet_Customer]
GO
