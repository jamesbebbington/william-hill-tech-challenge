﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using william_hill_tech_challenge.Utility;
using william_hill_tech_challenge.Data;
using william_hill_tech_challenge.Models;

namespace tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void ValidateFileLoader()
        {
            PrivateType pt = new PrivateType(typeof(FileLoader));

            FileLoader.LoadedData result = (FileLoader.LoadedData)
                pt.InvokeStatic("LoadFileToMemory", new object[] { @"~\..\..\..\..\william-hill-tech-challenge\Input\settled.csv", true });
            
            Assert.AreEqual(50, result.Bets.Count());
            Assert.AreEqual(6, result.Customers.Count());
            Assert.AreEqual(10, result.Customers.First().NumBets);
            Assert.AreEqual(7, result.Customers.First().NumWins);
            Assert.AreEqual(490, result.Customers.First().TotalBetValue);
        }

        [TestMethod]
        public void ValidateRiskyRule()
        {
            // 100%

            Customer customer = GenerateCustomer(1f);
            Bet bet = GenerateBet(customer, true);

            CustomerModel custModel = new CustomerModel(customer);
            BetModel betModel = new BetModel(bet, customer);

            Assert.AreEqual(true, custModel.Risky);
            Assert.AreEqual(true, betModel.Risky);

            // 100 > .. > 60

            customer = GenerateCustomer(0.7556f);
            bet = GenerateBet(customer, false);

            custModel = new CustomerModel(customer);
            betModel = new BetModel(bet, customer);

            Assert.AreEqual(true, custModel.Risky);
            Assert.AreEqual(true, betModel.Risky);

            // 60%

            customer = GenerateCustomer(0.6f);
            bet = GenerateBet(customer, true);

            custModel = new CustomerModel(customer);
            betModel = new BetModel(bet, customer);

            Assert.AreEqual(false, custModel.Risky);
            Assert.AreEqual(false, betModel.Risky);

            // 60 > .. > 0

            customer = GenerateCustomer(0.3333333f);
            bet = GenerateBet(customer, false);

            custModel = new CustomerModel(customer);
            betModel = new BetModel(bet, customer);

            Assert.AreEqual(false, custModel.Risky);
            Assert.AreEqual(false, betModel.Risky);

            // 0%

            customer = GenerateCustomer(0f);
            bet = GenerateBet(customer, true);

            custModel = new CustomerModel(customer);
            betModel = new BetModel(bet, customer);

            Assert.AreEqual(false, custModel.Risky);
            Assert.AreEqual(false, betModel.Risky);
        }

        [TestMethod]
        public void ValidateUnusualRules()
        {
            // < 10x

            Customer customer = GenerateCustomer(0.5f, 100);
            Bet bet = GenerateBet(customer, false, 1);

            BetModel betModel = new BetModel(bet, customer);

            Assert.AreEqual(false, betModel.Unusual);
            Assert.AreEqual(false, betModel.HighlyUnusual);

            // = 10x

            customer = GenerateCustomer(0.5f, 100);
            bet = GenerateBet(customer, false, 1000);

            betModel = new BetModel(bet, customer);

            Assert.AreEqual(false, betModel.Unusual);
            Assert.AreEqual(false, betModel.HighlyUnusual);

            // 10x < .. < 30x

            customer = GenerateCustomer(0.5f, 100);
            bet = GenerateBet(customer, false, 2000);

            betModel = new BetModel(bet, customer);

            Assert.AreEqual(true, betModel.Unusual);
            Assert.AreEqual(false, betModel.HighlyUnusual);

            // 30x

            customer = GenerateCustomer(0.5f, 100);
            bet = GenerateBet(customer, false, 3000);

            betModel = new BetModel(bet, customer);

            Assert.AreEqual(true, betModel.Unusual);
            Assert.AreEqual(false, betModel.HighlyUnusual);

            // > 30x

            customer = GenerateCustomer(0.5f, 100);
            bet = GenerateBet(customer, false, 10000);

            betModel = new BetModel(bet, customer);

            Assert.AreEqual(true, betModel.Unusual);
            Assert.AreEqual(true, betModel.HighlyUnusual);
        }

        [TestMethod]
        public void ValidateLargeBetRule()
        {
            // < 1000

            Customer customer = GenerateCustomer(0.5f);
            Bet bet = GenerateBet(customer, false, 100, 1);

            BetModel betModel = new BetModel(bet, customer);

            Assert.AreEqual(false, betModel.LargeBet);

            // 1000

            customer = GenerateCustomer(0.5f);
            bet = GenerateBet(customer, false, 100, 1000);

            betModel = new BetModel(bet, customer);

            Assert.AreEqual(true, betModel.LargeBet);

            // > 1000

            customer = GenerateCustomer(0.5f);
            bet = GenerateBet(customer, false, 100, 10000);

            betModel = new BetModel(bet, customer);

            Assert.AreEqual(true, betModel.LargeBet);
        }

        private Customer GenerateCustomer(float winPercentage, decimal aveBet = 100)
        {
            return new Customer
            {
                CustomerId = 1,
                NumBets = 100,
                NumWins = (int) Math.Round(100 * winPercentage),
                TotalBetValue = 100 * aveBet
            };
        }

        private Bet GenerateBet(Customer cust, bool settled, int amount = 100, int winValue = 500)
        {
            return new Bet
            {
                BetId = 1,
                Settled = settled,
                CustomerId = cust.CustomerId,
                EventId = 1,
                ParticipantId = 1,
                Stake = amount,
                WinValue = winValue
            };
        }
    }
}
